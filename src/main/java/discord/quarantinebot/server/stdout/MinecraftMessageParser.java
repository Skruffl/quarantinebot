package discord.quarantinebot.server.stdout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import discord.quarantinebot.Main;
import discord.quarantinebot.server.event.EventType;
import lombok.extern.slf4j.Slf4j;

/**
 * parses messages from minecrafts stdout
 */
@Slf4j
public class MinecraftMessageParser {

    private static final Pattern LOG_MESSAGE_END = Pattern.compile("\\]\\:\\ ");
    private static final Pattern LOG_MESSAGE_START = Pattern.compile("\\[/");
    private static final Pattern CHAT_MESSAGE = Pattern.compile("^\\<.*\\>.*");
    private Pattern CHAT_MESSAGE_END = Pattern.compile("\\>.*");

    private static MinecraftMessageParser instance;

    private List<String> deathMessages;

    public static MinecraftMessageParser getInstance() {
        if (instance == null) {
            instance = new MinecraftMessageParser();
        }
        return instance;
    }

    private MinecraftMessageParser() {
        log.info("Created new Minecraft Message Parser");
    }

    /**
     * parses a line from stdout (the minecraft log) and parses it into a parser
     * reply, which is turned into an event by the server
     * 
     * @param line the line to parse
     * @return the parser reply
     */
    public ParserReply parse(String line) {
        if (!line.contains("Server thread/INFO") && !line.contains("Chat Thread")) {
            return ParserReply.EMPTY;
        }
        line = cut(line);
        String death = getDeathMessage(line);
        // join
        if (line.contains("logged in with")) {
            String player = LOG_MESSAGE_START.split(line)[0].trim();
            return new ParserReply(EventType.JOIN, player, null);
        }
        // leave
        else if (line.contains("left the game")) {
            String player = line.replace(" left the game", "").trim();
            return new ParserReply(EventType.LEAVE, player, null);
        } else if (line.contains("has made the advancement")) {
            String[] split = line.split("has made the advancement");
            String advancement = split[1].replace("[", "").replace("]", "").trim();
            return new ParserReply(EventType.ADVANCEMENT, split[0].trim(), advancement);
        }
        // chat mesasge
        else if (CHAT_MESSAGE.matcher(line).matches()) {
            String player = CHAT_MESSAGE_END.matcher(line).replaceAll("").replace("<", "").trim();
            String message = line.replace("<" + player + "> ", "");
            return new ParserReply(EventType.CHAT, player, message);
        }
        // death message
        else if (death != null) {
            // get rid of extra junk on a villager death
            Pattern p = Pattern.compile("\'.*?\'");
            Matcher matcher = p.matcher(line);
            while (matcher.find()) {
                String group = matcher.group();
                if (!group.contains(death)) {
                    continue;
                }
                line = group.replace("'", "");
                break;
            }

            String player = line.split(death)[0].trim();
            death = line.replace(player + " ", "");
            return new ParserReply(EventType.DEATH, player, death);
        } else {
            return ParserReply.EMPTY;
        }
    }

    /**
     * checks if the line contains a default death message and returns the clean
     * death message
     * 
     * @param line the line to check
     * @return the death line or null if no match was found
     */
    private String getDeathMessage(String line) {
        if (deathMessages == null) {
            deathMessages = new ArrayList<>();
            URL resource = Main.class.getResource("/deaths.txt");
            log.info("Using resource {} as string {}", resource.toExternalForm(), resource.toString());
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.openStream()))) {
                String read = "";
                while ((read = reader.readLine()) != null) {
                    deathMessages.add(read);
                }
            } catch (IOException e) {
                log.error("Couldn't read death messages file", e);
            }
        }

        for (String death : deathMessages) {
            // the deaths.txt file contains [player] at the position the player name would
            // be inserted
            death = death.replace("[player] ", "");
            if (line.contains(death)) {
                return death;
            }
        }
        return null;
    }

    /**
     * removes the log timestamp and the thread name at the beginning of the line
     * 
     * @param line the line to cut from
     * @return the clean line
     */
    private String cut(String line) {
        return LOG_MESSAGE_END.split(line)[1];
    }

}