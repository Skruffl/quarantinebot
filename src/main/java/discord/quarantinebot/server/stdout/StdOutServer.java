package discord.quarantinebot.server.stdout;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import discord.quarantinebot.server.Server;
import discord.quarantinebot.server.event.MinecraftListener;
import lombok.extern.slf4j.Slf4j;

/**
 * StdOutServer uses the standard channels (StdIn and StdOut) in order to send
 * and receive messages from the minecraft server. This works without Bukkit and
 * can be used for Vanilla Servers.
 */
@Slf4j
public class StdOutServer implements Server {
    private Process process;
    private List<MinecraftListener> events;

    /**
     * creates a new Standard-Out Based Server. This Server instance will use the
     * arguments passed on the command line to start a minecraft server (for example
     * <code>java -jar server.jar --nogui</code>) and it'll read from standard out
     * and write to standard in in order to hook into the minecraft chat
     * 
     * @param args the command line arguments to start the minecraft server with
     */
    public StdOutServer(String[] args) {
        events = new ArrayList<>();
        ProcessBuilder builder = new ProcessBuilder(args);
        try {
            process = builder.start();
            new Thread(() -> readStream(process.getInputStream())).start();
            new Thread(() -> readStream(process.getErrorStream())).start();
            new Thread(() -> consoleProxy(System.in)).start();

            // Wait for server to start
            Thread.sleep(5000L);
        } catch (IOException e) {
            log.error("Could not start minecraft server", e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error("Interrupted", e);
        }

        process.onExit().thenAccept(e -> System.exit(0));

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                close();
            } catch (IOException e) {
                log.error("Error while shutting down bot", e);
            }
        }));
    }

    /**
     * reads the input to the bot and forwards it to the minecraft server
     * 
     * @param in the standard input
     */
    private void consoleProxy(InputStream in) {
        try (Scanner scanner = new Scanner(in)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine() + "\r\n";
                OutputStream outputStream = process.getOutputStream();
                outputStream.write(line.getBytes(StandardCharsets.UTF_8));
                outputStream.flush();
            }
        } catch (IOException e) {
            log.error("Output error while trying to write to Minecraft Server STDOUT", e);
        }
    }

    /**
     * called in the constructor in an extra thread. reads from stdout as long as
     * the channel is open and parses each line received.
     */
    private void readStream(InputStream stream) {
        try (Scanner scanner = new Scanner(stream)) {
            while (scanner.hasNextLine()) {
                eventQueue(scanner.nextLine());
            }
        }
    }

    /**
     * handles the event queue for each line received from stdout
     * 
     * @param line the line to parse
     */
    private void eventQueue(String line) {
        System.out.println(line);
        Thread thread = new Thread(() -> {
            ParserReply parse = null;
            try {
                parse = MinecraftMessageParser.getInstance().parse(line);
            } catch (Exception e) {
                log.error("Parsing error", e);
            }
            for (MinecraftListener event : events) {
                if (event.getEventType() == parse.getType()) {
                    event.genericMessage(parse.getPlayer(), parse.getMessage());
                }
            }
        });
        thread.setName("Minecraft Event Thread");
        thread.start();
    }

    public void addEventListener(MinecraftListener e) {
        events.add(e);
    }

    public void sendMessage(String player, String message) {
        PrintWriter printWriter = new PrintWriter(process.getOutputStream());

        JsonArray array = new JsonArray();
        array.add(createJsonObject("<" + player + "> "));
        array.add(createJsonObject(message));

        String msgTxt = "tellraw @a " + array.toString();
        log.debug("Sending tellraw: {}", msgTxt);
        printWriter.println(msgTxt);
        printWriter.flush();
    }

    private JsonObject createJsonObject(String text) {
        text = text.replace("§", "");
        JsonObject name = new JsonObject();
        name.addProperty("text", text);
        return name;
    }

    public void sendCommand(String cmd) {
        PrintWriter printWriter = new PrintWriter(process.getOutputStream());
        printWriter.println(cmd);
        printWriter.flush();
    }

    @Override
    public void close() throws IOException {
        process.destroy();
    }
}