package discord.quarantinebot.server.stdout;

import lombok.Getter;
import lombok.ToString;
import discord.quarantinebot.server.event.EventType;

@Getter
@ToString
public class ParserReply {
    public static final ParserReply EMPTY = new ParserReply(null, null, null);

    private EventType type;
    private String player;
    private String message;

    public ParserReply(EventType type, String player, String message) {
        this.type = type;
        this.player = player;
        this.message = message;
    }
}