package discord.quarantinebot.server.event;

public abstract class MinecraftLeaveListener implements MinecraftListener {

    @Override
    public EventType getEventType() {
        return EventType.LEAVE;
    }

    @Override
    public void genericMessage(String player, String message) {
        playerLeft(player);
    }

    public abstract void playerLeft(String player);

}