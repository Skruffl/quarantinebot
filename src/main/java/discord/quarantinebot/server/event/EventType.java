package discord.quarantinebot.server.event;

public enum EventType {
    CHAT, DEATH, JOIN, LEAVE, ADVANCEMENT;
}