package discord.quarantinebot.server.event;

public abstract class MinecraftAdvancementListener implements MinecraftListener {

    @Override
    public EventType getEventType() {
        return EventType.ADVANCEMENT;
    }

    @Override
    public void genericMessage(String player, String message) {
        advancementMade(player, message);
    }

    protected abstract void advancementMade(String player, String message);

}
