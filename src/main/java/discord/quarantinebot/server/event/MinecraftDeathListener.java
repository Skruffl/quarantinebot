package discord.quarantinebot.server.event;

public abstract class MinecraftDeathListener implements MinecraftListener {
    @Override
    public EventType getEventType() {
        return EventType.DEATH;
    }

    @Override
    public void genericMessage(String player, String message) {
        deathOccurred(player, message);
    }

    public abstract void deathOccurred(String player, String message);
}