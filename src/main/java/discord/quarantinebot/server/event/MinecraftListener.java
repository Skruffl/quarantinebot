package discord.quarantinebot.server.event;

public interface MinecraftListener {

    /**
     * returns the event type of this event
     * 
     * @return the event type
     */
    EventType getEventType();

    /**
     * receive a generic message event
     * 
     * @param player  the player involved
     * @param message the message of the player
     */
    void genericMessage(String player, String message);
}