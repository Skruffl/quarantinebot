package discord.quarantinebot.server.event;

public abstract class MinecraftChatListener implements MinecraftListener {

    @Override
    public EventType getEventType() {
        return EventType.CHAT;
    }

    @Override
    public void genericMessage(String player, String message) {
        chatMessageSent(player, message);
    }

    public abstract void chatMessageSent(String player, String message);
}