package discord.quarantinebot.server.event;

public abstract class MinecraftJoinListener implements MinecraftListener {

    @Override
    public EventType getEventType() {
        return EventType.JOIN;
    }

    @Override
    public void genericMessage(String player, String message) {
        playerJoined(player);
    }

    public abstract void playerJoined(String player);

}