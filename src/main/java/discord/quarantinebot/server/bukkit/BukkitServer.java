package discord.quarantinebot.server.bukkit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import discord.quarantinebot.bot.Bot;
import discord.quarantinebot.server.Server;
import discord.quarantinebot.server.event.EventType;
import discord.quarantinebot.server.event.MinecraftListener;
import discord.quarantinebot.server.stdout.ParserReply;
import lombok.extern.slf4j.Slf4j;

/**
 * the bukkit plugin class, handling the
 */
@Slf4j
public class BukkitServer extends JavaPlugin implements Server, Listener {

    private Bot bot;
    private List<MinecraftListener> events;

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        events = new ArrayList<>();
        bot = new Bot(this);
    }

    @Override
    public void onDisable() {
        try {
            close();
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Couldn't shut down bot", e);
        }
    }

    @Override
    public void close() throws IOException {
        bot.shutdown();
    }

    @Override
    public void sendCommand(String command) {
        getLogger().info("Executing command " + command);
        BukkitRunnable run = new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);
                } catch (Exception e) {
                    // Do nothing
                }
            }
        };
        run.runTask(this);
    }

    @Override
    public void sendMessage(String player, String message) {
        JsonArray array = new JsonArray();
        array.add(createJsonObject("<" + player + "> "));
        array.add(createJsonObject(message));

        String msg = "tellraw @a " + array.toString();
        log.debug("Sending tellraw: {}", msg);
        sendCommand(msg);
    }

    private JsonObject createJsonObject(String text) {
        text = text.replace("§", "");
        JsonObject name = new JsonObject();
        name.addProperty("text", text);
        return name;
    }

    @Override
    public void addEventListener(MinecraftListener e) {
        events.add(e);
    }

    /**
     * handles the event queue for the events sent by bukkit
     * 
     * @param reply the reply to send
     */
    private void eventQueue(ParserReply reply) {
        getLogger().info(String.format("Received ParserReply: %s", reply.toString()));
        Thread thread = new Thread(() -> {
            for (MinecraftListener event : events) {
                if (event.getEventType() == reply.getType()) {
                    event.genericMessage(reply.getPlayer(), reply.getMessage());
                }
            }
        });
        thread.setName("Minecraft Event Thread");
        thread.start();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        getLogger().info(String.format("Player %s joined!", e.getPlayer().getName()));
        eventQueue(new ParserReply(EventType.JOIN, e.getPlayer().getName(), null));
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        getLogger().info(String.format("Player %s left!", e.getPlayer().getName()));
        eventQueue(new ParserReply(EventType.LEAVE, e.getPlayer().getName(), null));
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        getLogger().info(String.format("Player %s died!", e.getEntity().getName()));
        eventQueue(new ParserReply(EventType.DEATH, e.getEntity().getName(),
                e.getDeathMessage().replace(e.getEntity().getName(), "").trim()));
    }

    @EventHandler
    public void onMessage(AsyncPlayerChatEvent e) {
        getLogger().info(String.format("Player %s said %s!", e.getPlayer().getName(), e.getMessage()));
        eventQueue(new ParserReply(EventType.CHAT, e.getPlayer().getName(), e.getMessage()));
    }

}