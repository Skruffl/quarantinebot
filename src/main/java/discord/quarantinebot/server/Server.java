package discord.quarantinebot.server;

import java.io.Closeable;

import discord.quarantinebot.server.event.MinecraftListener;

public interface Server extends Closeable {
    /**
     * sends a command to the underlying minecraft server
     * 
     * @param command the command to execcute
     */
    public void sendCommand(String command);

    /**
     * sends a message to the chat of the underlying minecraft server
     * 
     * @param player  the (discord) player to send the message as
     * @param message the message
     */
    public void sendMessage(String player, String message);

    /**
     * register an event handler
     * 
     * @param e the event handler to register
     */
    public void addEventListener(MinecraftListener e);
}