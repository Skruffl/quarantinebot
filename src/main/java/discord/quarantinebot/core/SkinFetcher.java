package discord.quarantinebot.core;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Base64;
import java.util.Collections;
import java.util.Base64.Decoder;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import com.google.gson.Gson;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import discord.quarantinebot.bot.Bot;
import discord.quarantinebot.core.mojangapi.MinecraftProfile;
import discord.quarantinebot.core.mojangapi.Profile;
import discord.quarantinebot.core.mojangapi.Properties;
import discord.quarantinebot.core.mojangapi.Value;
import lombok.extern.slf4j.Slf4j;

/**
 * SkinFetcher is used to interact with the mojang api in order to pull the
 * minecraft skin for a specific person. the result will return a discord icon
 * cropped to the face-pixels of the skin scaled up to {@link this#IMAGE_SIZE}
 */
@Slf4j
public class SkinFetcher {
    private static final int IMAGE_SIZE = 128;
    public static final String SERVER_USER = "Server";
    private Gson gson = new Gson();

    private Map<String, IconHolder> iconMap;
    private Bot bot;

    public SkinFetcher(Bot bot) {
        this.bot = bot;
        log.info("Created new Skin Fetcher!");
        iconMap = Collections.synchronizedMap(new HashMap<>());
        try {
            iconMap.put(SERVER_USER, IconHolder.from(SERVER_USER, getClass().getResourceAsStream("/server.png"), bot));
        } catch (IOException e) {
            log.error("Couldn't load server icon", e);
        }
    }

    /**
     * fetches the face-icon of the player and saves it in a cache
     * 
     * @param username the username to load the skin for
     * @return the icon
     */
    public IconHolder getIcon(String username) {
        IconHolder holder = iconMap.computeIfAbsent(username, name -> IconHolder.from(name, fetchIcon(name), bot));

        // refetch icon if it is invalid
        if (!holder.isValid()) {
            iconMap.put(username, IconHolder.from(username, fetchIcon(username), bot));
        }

        return holder;
    }

    /**
     * fetches the icon from the mojang api
     * 
     * @param username the username
     * @return the icon
     */
    private byte[] fetchIcon(String username) {
        Decoder decoder = Base64.getDecoder();
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            MinecraftProfile mcProfile = getMcProfile(username, client);
            Profile profile = getProfile(mcProfile, client);
            Properties[] properties = profile.getProperties();
            String profileValue = properties[0].getValue();
            Value value = gson.fromJson(new String(decoder.decode(profileValue)), Value.class);
            return getIconFromUrl(value.getTextures().getSKIN().getUrl());
        } catch (IOException e) {
            log.error("Couldn't close http client", e);
        }
        return null;
    }

    /**
     * downloads the skin from the api url and crops it down to the face
     * 
     * @param url the url to pull form
     * @return the icon
     */
    private byte[] getIconFromUrl(String url) {
        try {
            // full skin
            BufferedImage skin = ImageIO.read(new URL(url));
            // the actual face
            BufferedImage face = skin.getSubimage(8, 8, 8, 8);
            // the overlay layer (hats/hair/whatever)
            BufferedImage overlay = skin.getSubimage(40, 8, 8, 8);

            // new, upscaled image
            BufferedImage icon = new BufferedImage(IMAGE_SIZE, IMAGE_SIZE, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = icon.createGraphics();
            g.drawImage(face, 0, 0, IMAGE_SIZE, IMAGE_SIZE, null); // draws the face and scales it up
            g.drawImage(overlay, 0, 0, IMAGE_SIZE, IMAGE_SIZE, null); // overlays the overaly and scales it up
            g.dispose();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(icon, "jpeg", baos);
            return baos.toByteArray();
        } catch (IOException e) {
            log.error("Couldn't load image from url", e);
        }
        return null;
    }

    /**
     * loads the profile (contains most of the important information)
     * 
     * @param mcProfile
     * @param client
     * @return the profile
     */
    private Profile getProfile(MinecraftProfile mcProfile, CloseableHttpClient client) {
        HttpGet get = new HttpGet("https://sessionserver.mojang.com/session/minecraft/profile/" + mcProfile.getId());
        return gson.fromJson(readRequest(client, get), Profile.class);
    }

    /**
     * loads the minecraft profile (contains id and name)
     * 
     * @param username
     * @param client
     * @return
     */
    private MinecraftProfile getMcProfile(String username, CloseableHttpClient client) {
        HttpGet get = new HttpGet("https://api.mojang.com/users/profiles/minecraft/" + username);
        return gson.fromJson(readRequest(client, get), MinecraftProfile.class);
    }

    /**
     * executes a webrequest and reads all lines it returns and returns those as a
     * string
     * 
     * @param client the http client to use
     * @param req    the request to send over the client
     * @return the response as a string
     */
    private String readRequest(HttpClient client, HttpUriRequest req) {
        try {
            String content = new BufferedReader(new InputStreamReader(client.execute(req).getEntity().getContent()))
                    .lines().collect(Collectors.joining("\n"));
            return content;
        } catch (UnsupportedOperationException | IOException e) {
            log.error("Couldn't send http request", e);
            return null;
        }
    }
}