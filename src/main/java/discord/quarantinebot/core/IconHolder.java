package discord.quarantinebot.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import discord.quarantinebot.bot.Bot;
import lombok.Getter;
import lombok.ToString;
import net.dv8tion.jda.api.entities.Message;

@Getter
@ToString
public class IconHolder {
    private static final long TIMEOUT = TimeUnit.HOURS.toMillis(1);

    private long creationTime;
    private String name;
    private String iconUrl;

    public IconHolder(String name, String iconUrl) {
        this.name = name;
        this.iconUrl = iconUrl;
        creationTime = System.currentTimeMillis();
    }

    public static IconHolder from(String name, byte[] image, Bot bot) {
        Message msg = bot.getStorageChannel().sendFile(image, "image.jpg").complete();
        return new IconHolder(name, msg.getAttachments().get(0).getUrl());
    }

    public static IconHolder from(String name, InputStream io, Bot bot) throws IOException {
        Message msg = bot.getStorageChannel().sendFile(io, "image.jpg").complete();
        return new IconHolder(name, msg.getAttachments().get(0).getUrl());
    }

    /**
     * checks if the icon timed out and has to be reset
     */
    public boolean isValid() {
        return name.equals(SkinFetcher.SERVER_USER) || (creationTime + TIMEOUT) > System.currentTimeMillis();
    }

}
