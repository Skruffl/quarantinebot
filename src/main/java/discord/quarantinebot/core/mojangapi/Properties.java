package discord.quarantinebot.core.mojangapi;

import lombok.Getter;

@Getter
public class Properties {
    private String name;
    private String value;
    private String signature;
}