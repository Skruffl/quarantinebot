package discord.quarantinebot.core.mojangapi;

import lombok.Getter;

@Getter
public class Texture {

    private Skin SKIN;
    private Cape CAPE;

}