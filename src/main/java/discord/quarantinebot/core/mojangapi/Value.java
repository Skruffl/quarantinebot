package discord.quarantinebot.core.mojangapi;

import lombok.Getter;

@Getter
public class Value {
    private String timestamp;
    private String profileId;
    private String profileName;
    private boolean signatureRequired;
    private Texture textures;
}