package discord.quarantinebot.core.mojangapi;

import lombok.Getter;

@Getter
public class Skin {
    private String url;
}