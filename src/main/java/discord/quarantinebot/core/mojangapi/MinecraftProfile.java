package discord.quarantinebot.core.mojangapi;

import lombok.Getter;

@Getter
public class MinecraftProfile {
    private String id;
    private String name;
    private boolean legacy;
    private boolean demo;
}