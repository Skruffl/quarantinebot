package discord.quarantinebot.core.mojangapi;

import lombok.Getter;

@Getter
public class Profile {
    private String id;
    private String name;
    private Properties[] properties;
}