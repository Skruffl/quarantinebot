package discord.quarantinebot.bot;

import java.util.ArrayList;
import java.util.List;

import club.minnced.discord.webhook.send.WebhookMessage;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import discord.quarantinebot.core.IconHolder;
import discord.quarantinebot.core.SkinFetcher;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.entities.TextChannel;

@Slf4j
public class WebhookHandler {
    // this list is thread safe
    private List<WebhookHolder> webhooks;

    private Config config;

    private Bot bot;

    private int roundRobinHook = 0;

    public WebhookHandler(Bot bot) {
        this.bot = bot;
        config = Config.readConfig();
        webhooks = new ArrayList<>();
        createAndValidateWebhooks();
    }

    /**
     * checks if the config-specified channel has enough webhooks for the bot to
     * use. if not, we'll create them
     */
    private void createAndValidateWebhooks() {
        long channelId = config.getChannelId();
        TextChannel channel = bot.getJda().getGuildById(config.getServerId()).getTextChannelById(channelId);
        log.debug("Creating and validating webhooks in channel {}", channel.getId());

        // clean out all webhooks before starting
        channel.retrieveWebhooks().complete().stream()
                .filter(hook -> hook.getOwner().equals(bot.getJda().getGuildById(config.getServerId()).getSelfMember()))
                .forEachOrdered(hook -> hook.delete().complete());
        log.debug("Cleared all of our previously existing hooks!");

        while (webhooks.size() < config.getWebhookCount()) {
            log.debug("We only have {} webhooks, creating a new one!", webhooks.size());
            webhooks.add(new WebhookHolder(channel.createWebhook(SkinFetcher.SERVER_USER).complete()));
        }
        log.debug("We're done validating webhooks!");
    }

    /**
     * sends a message for the given minecraft player
     * 
     * @param player      the player
     * @param messageText the message text
     * @return the discord message id
     */
    public synchronized long sendMessage(String player, String messageText) {
        long started = System.currentTimeMillis();
        log.debug("Started processing message at {}", started);

        messageText = MessageSanitiser.getInstance().sanitiseMessage(messageText);

        WebhookHolder webhook = null;
        try {
            webhook = grabNextWebhook();

            IconHolder icon = bot.getSkinFetcher().getIcon(player);

            log.debug("Sending message {} for player {}", messageText, icon);

            // @formatter:off
            WebhookMessage msg = new WebhookMessageBuilder()
                .setContent(messageText)
                .setAvatarUrl(icon.getIconUrl())
                .setUsername(icon.getName())
                .build();
            // @formatter:on

            return webhook.getClient().send(msg).join().getId();
        } catch (Exception e) {
            log.error("Couldn't send webhook message", e);

            // assume the webhook we used is broken, so we remove and revalidate it
            if (webhook != null) {
                webhooks.remove(webhook);
            }
            createAndValidateWebhooks();

            // try again
            return sendMessage(player, messageText);
        } finally {
            long finished = System.currentTimeMillis();
            log.debug("Finished processing message at {}", finished);
            log.debug("That's {} milliseconds or {} seconds", (finished - started), (finished - started) / 1000);
        }
    }

    private synchronized WebhookHolder grabNextWebhook() {
        WebhookHolder webhookHolder = webhooks.get(roundRobinHook++);
        if (roundRobinHook >= webhooks.size()) {
            roundRobinHook = 0;
        }
        return webhookHolder;
    }

}
