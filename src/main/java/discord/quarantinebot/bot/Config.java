package discord.quarantinebot.bot;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * Config stores the config for the bot, this will end up in the quarantine.json
 * text file
 */
@Slf4j
@Data
public class Config {
    public static final String CONFIG_LOCATION = "quarantine.json";

    private long serverId;
    private long channelId;
    private String token = "";
    private String prefix = "!";
    private String deathEmoji = "🇫";
    private String address = "";
    private int webhookCount = 3;

    public static Config readConfig() {
        try {
            String config = new String(Files.readAllBytes(Paths.get(CONFIG_LOCATION)), StandardCharsets.UTF_8);
            return new Gson().fromJson(config, Config.class);
        } catch (IOException e) {
            new Config().writeConfig();
            throw new IllegalStateException("No config present. Please fill the dummy file before starting the bot.");
        }
    }

    public void writeConfig() {
        try {
            new File(CONFIG_LOCATION).delete();
            Files.write(Paths.get(CONFIG_LOCATION),
                    Arrays.asList(new GsonBuilder().setPrettyPrinting().create().toJson(this).split("\\n")),
                    StandardCharsets.UTF_8, StandardOpenOption.CREATE_NEW);
        } catch (IOException e) {
            log.error("Could not write config file", e);
        }
    }

}