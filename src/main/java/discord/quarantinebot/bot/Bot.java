package discord.quarantinebot.bot;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

import javax.security.auth.login.LoginException;

import discord.quarantinebot.core.SkinFetcher;
import discord.quarantinebot.server.Server;
import discord.quarantinebot.server.event.MinecraftAdvancementListener;
import discord.quarantinebot.server.event.MinecraftChatListener;
import discord.quarantinebot.server.event.MinecraftDeathListener;
import discord.quarantinebot.server.event.MinecraftJoinListener;
import discord.quarantinebot.server.event.MinecraftLeaveListener;
import discord.quarantinebot.server.stdout.StdOutServer;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

/**
 * Bot is used to start and control the actual discord bot
 */
@Slf4j
@Getter
public class Bot extends ListenerAdapter {
    private Server server;

    private Config config;
    private JDA jda;
    private SkinFetcher skinFetcher;

    private WebhookHandler webhookHandler;

    private TextChannel storageChannel;
    private TextChannel textChannel;

    private static final String STORAGE_CHANNEL_NAME = "quarantinebot-temp-storage";

    private Set<String> connectedPlayers;

    public Bot(Server server) {
        this.server = server;
        this.connectedPlayers = new HashSet<>();
        config = Config.readConfig();

        try {
            String token = config.getToken();
            jda = JDABuilder
                    .create(GatewayIntent.GUILD_WEBHOOKS, GatewayIntent.GUILD_MESSAGE_REACTIONS,
                            GatewayIntent.GUILD_EMOJIS, GatewayIntent.GUILD_MESSAGES)
                    .disableCache(CacheFlag.ACTIVITY, CacheFlag.VOICE_STATE, CacheFlag.CLIENT_STATUS).setToken(token)
                    .build().awaitReady();
        } catch (LoginException | InterruptedException e) {
            log.error("Couldn't log into discord api!", e);
        }
        jda.addEventListener(this);

        this.textChannel = jda.getTextChannelById(config.getChannelId());
        createStorageChannel();
        webhookHandler = new WebhookHandler(this);
        registerEventHandlers();
        skinFetcher = new SkinFetcher(this);

        if (config.getServerId() == 0) {
            log.info("******************************************************************");
            log.info("******************************************************************");
            log.info("********Successfully started discord bot! Please specify a********");
            log.info("****mirror channel by sending !channel in the desired channel.****");
            log.info("******************************************************************");
            log.info("******************************************************************");
        }
        log.debug("Bot booted up!");
    }

    private void createStorageChannel() {
        Guild guild = jda.getGuildById(config.getServerId());
        List<TextChannel> channels = guild.getTextChannelsByName(STORAGE_CHANNEL_NAME, true);
        if (!channels.isEmpty()) {
            storageChannel = channels.get(0);
            storageChannel.getIterableHistory().stream().forEach(msg -> msg.delete().queue());
        } else {
            storageChannel = guild.createTextChannel(STORAGE_CHANNEL_NAME)
                    .addRolePermissionOverride(guild.getPublicRole().getIdLong(), Collections.emptyList(),
                            Arrays.asList(Permission.MESSAGE_READ))
                    .complete();
        }
    }

    /**
     * registers all event handlers we could possibly listen for
     */
    private void registerEventHandlers() {
        log.debug("Registering event handlers!");
        server.addEventListener(new MinecraftChatListener() {
            @Override
            public void chatMessageSent(String player, String message) {
                sendMessage(player, message);
            }
        });
        server.addEventListener(new MinecraftDeathListener() {
            @Override
            public void deathOccurred(String player, String message) {
                long messageId = sendMessage(SkinFetcher.SERVER_USER, "**" + player + "** " + message);
                react(config.getDeathEmoji(), messageId);
            }

        });
        server.addEventListener(new MinecraftJoinListener() {
            @Override
            public void playerJoined(String player) {
                connectedPlayers.add(player);
                sendMessage(SkinFetcher.SERVER_USER, "**" + player + "**  joined the server!");
            }
        });
        server.addEventListener(new MinecraftLeaveListener() {
            @Override
            public void playerLeft(String player) {
                connectedPlayers.remove(player);
                sendMessage(SkinFetcher.SERVER_USER, "**" + player + "** left the server!");
            }
        });

        server.addEventListener(new MinecraftAdvancementListener() {
            @Override
            protected void advancementMade(String player, String message) {
                sendMessage(SkinFetcher.SERVER_USER, "**" + player + "** has made the advancement **" + message + "**");
            }
        });
    }

    private String generatePlayerList() {
        StringJoiner joiner = new StringJoiner(", ");
        log.debug("Generating player list, connected player count: {}", connectedPlayers.size());
        for (String player : connectedPlayers) {
            log.debug("Adding player {}", player);
            joiner.add(player);
        }

        if (joiner.toString().isEmpty()) {
            return "Players: None :(";
        }

        return "Players: " + joiner.toString();
    }

    /**
     * reacts to a message with the given emoji
     * 
     * @param emoji     the emoji to react with
     * @param messageId the message to react to
     */
    private void react(String emoji, long messageId) {
        if (messageId == 0l) {
            return;
        }
        // if its a custom emoji -> contains id of the emoji
        if (emoji.matches("\\d*")) {
            jda.getGuildById(config.getServerId()).getTextChannelById(config.getChannelId())
                    .addReactionById(messageId, jda.getEmoteById(emoji)).queue();
        } else if (!emoji.isEmpty()) {
            // a unicode emoji
            jda.getGuildById(config.getServerId()).getTextChannelById(config.getChannelId())
                    .addReactionById(messageId, emoji).queue();
        }
    }

    /**
     * sends a message to the discord channel
     * 
     * @param player the player name to send for
     * @param msg    the message to send
     */
    private long sendMessage(String player, String msg) {
        return webhookHandler.sendMessage(player, msg);
    }

    /**
     * called when the bot receives a new message in discord
     * 
     * @param event the event
     */
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        // return if bot
        if (event.getAuthor().isBot()) {
            return;
        }

        // command check
        if (event.getMessage().getContentRaw().startsWith(config.getPrefix())) {
            command(event);
            return;
        }

        // return if we're not in the specified channel
        if (event.getGuild().getIdLong() != config.getServerId()
                || event.getChannel().getIdLong() != config.getChannelId()) {
            return;
        }

        // Send minecraft message
        String nickname = event.getMember().getEffectiveName();
        String msg = event.getMessage().getContentDisplay();
        if (!event.getMessage().getAttachments().isEmpty()) {
            msg += "    <Contains attachment>";
        }
        for (String message : msg.split("\n")) {
            server.sendMessage(nickname, message.trim());
        }
    }

    /**
     * simple command handler called if the bot finds the prefix in a message
     * 
     * @param event the message containing the command
     */
    private void command(MessageReceivedEvent event) {
        String messageContent = event.getMessage().getContentRaw();
        String[] args = messageContent.split(" ", 2);
        String command = args[0].replace(config.getPrefix(), "");
        String content = "";
        if (args.length >= 2) {
            content = args[1];
        }

        switch (command) {
            case "channel":
                channelCommand(event);
                break;
            case "emoji":
                emojiCommand(event, content);
                break;
            case "stop":
                stopCommand(event);
                break;
            case "command":
                server.sendCommand(content.trim());
                break;
            case "ip":
            case "ipaddr":
            case "address":
                addressCommand(event);
                break;

            case "players":
                playersCommand(event);
                break;

            default:
                return;
        }
    }

    private void playersCommand(MessageReceivedEvent event) {
        event.getChannel().sendMessage(generatePlayerList()).queue();
    }

    private void addressCommand(MessageReceivedEvent event) {
        if (event.getMember().isOwner()) {
            String[] content = event.getMessage().getContentRaw().split(" ");
            if (content.length > 1) {
                StringBuilder builder = new StringBuilder();
                for (int i = 1; i < content.length; i++) {
                    builder.append(content[i]);
                }
                config.setAddress(builder.toString());
                config.writeConfig();

                textChannel.getManager().setTopic("IP Address: " + config.getAddress()).queue();
                event.getChannel().sendMessage("Updated IP Address! :thumbsup:").queue();
            }
        }

        if (config.getAddress() == null || config.getAddress().trim().isEmpty()) {
            event.getChannel().sendMessage("No IP Address was configured...").queue();
        } else {
            event.getChannel().sendMessage("The IP Address is `" + config.getAddress() + "`").queue();
        }
    }

    /**
     * the emoji command, used to change the emoji we're adding to any death message
     * 
     * @param event   the event that caused this
     * @param content the message content (the emoji)
     */
    private void emojiCommand(MessageReceivedEvent event, String content) {
        if (!event.getMember().isOwner()) {
            event.getChannel().sendMessage("You have to be the guild owner to do this!").queue();
            return;
        }
        List<Emote> emotes = event.getMessage().getEmotes();
        String emote = "";
        if (!emotes.isEmpty()) {
            emote = emotes.get(0).getId();
        } else {
            emote = content;
        }
        config.setDeathEmoji(emote);
        event.getChannel().sendMessage(String.format("Updated death emoji to %s :thumbsup:", content)).queue();
    }

    /**
     * the channel command, used to change the channel we're listening to
     * 
     * @param event the event that caused this
     */
    private void channelCommand(MessageReceivedEvent event) {
        if (!event.getMember().isOwner()) {
            event.getChannel().sendMessage("You have to be the guild owner to do this!").queue();
            return;
        }
        config.setChannelId(event.getChannel().getIdLong());
        config.setServerId(event.getGuild().getIdLong());
        config.writeConfig();
        event.getChannel().sendMessage("Updated Channel!").queue();
    }

    /**
     * the stop command used to shut down the minecraft server (only works with
     * {@link StdOutServer})
     * 
     * @param event the event that caused this
     */
    private void stopCommand(MessageReceivedEvent event) {
        if (!event.getMember().isOwner()) {
            event.getChannel().sendMessage("You have to be the guild owner to do this!").queue();
            return;
        }
        try {
            server.close();
        } catch (IOException e) {
            log.error("Couldn't close server", e);
        }
        shutdown();
        config.writeConfig();
        System.exit(0);
    }

    /**
     * shuts down the bot
     */
    public void shutdown() {
        storageChannel.delete().complete();
        jda.shutdownNow();
    }

}
