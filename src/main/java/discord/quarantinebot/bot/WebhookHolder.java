package discord.quarantinebot.bot;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.WebhookClientBuilder;
import discord.quarantinebot.core.IconHolder;
import lombok.Getter;
import net.dv8tion.jda.api.entities.Webhook;

@Getter
public class WebhookHolder {
    private Webhook webhook;
    private WebhookClient client;
    private IconHolder icon;

    public WebhookHolder(Webhook webhook) {
        this.webhook = webhook;
        client = new WebhookClientBuilder(webhook.getIdLong(), webhook.getToken()).build();
    }

    public void setIcon(IconHolder icon) {
        if (this.icon == null || !this.icon.equals(icon)) {
            this.icon = icon;
        }
    }
}
