package discord.quarantinebot.bot;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageSanitiser {

    private static final MessageSanitiser INSTANCE = new MessageSanitiser();

    private static final List<String> GENERIC_ROLES = Arrays.asList("everyone", "here");

    private MessageSanitiser() {
    }

    public static MessageSanitiser getInstance() {
        return INSTANCE;
    }

    public String sanitiseMessage(String messageText) {
        messageText = sanitiseGenericRoles(messageText);
        return sanitiseSpecificRoles(messageText);
    }

    private String sanitiseSpecificRoles(String messageText) {
        Pattern p = Pattern.compile("\\<\\@\\&\\d+\\>");
        Matcher m = p.matcher(messageText);
        while (m.find()) {
            String groupMatch = m.group();
            String groupSubstitute = groupMatch.replace("@", "/@");
            messageText = messageText.replace(groupMatch, groupSubstitute);
        }
        return messageText;
    }

    private String sanitiseGenericRoles(String messageText) {
        for (String role : GENERIC_ROLES) {
            if (messageText.contains("@" + role)) {
                messageText = messageText.replace("@" + role, "@/" + role);
            }
        }
        return messageText;
    }

}
