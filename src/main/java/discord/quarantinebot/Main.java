package discord.quarantinebot;

import java.io.File;

import discord.quarantinebot.bot.Bot;
import discord.quarantinebot.bot.Config;
import discord.quarantinebot.server.stdout.StdOutServer;

public class Main {
    public static void main(String[] args) {
        if (args.length <= 0 || !new File(Config.CONFIG_LOCATION).exists()) {
            System.out.println("Writing new config!");
            new Config().writeConfig();
            return;
        }
        new Bot(new StdOutServer(args));
    }
}