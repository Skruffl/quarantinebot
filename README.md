Quarantinebot
-----
 A Discord bot allowing you to mirror a Minecraft Server Chat into a Discord Chat.
 Written in Java using JDA.


Compilation
-----
You'll need a JDK >= 8 and Maven (mvn)

```
git clone https://gitlab.com/Skruffl/quarantinebot
mvn install -f  ./quarantinebot/pom.xml
```

The Jarfile will be available here: `quarantinebot/target/quarantinebot-{version}-SNAPSHOT-jar-with-dependencies.jar`

Installation and Setup
-----
Download the most recent Release from [the Releases page](https://gitlab.com/Skruffl/quarantinebot/-/releases)!

There's two installation methods available.

1. Bukkit Plugin
The Jarfile is a Bukkit Plugin you can just drop into the Plugins-Folder of your Bukkit Installation.
It'll create the config file `quarantine.json` in the same directory as the Minecraft Server is.

2. Vanilla Server
You can use this bot by letting it read the Standard Output and Standard Input of the Minecraft Server.
For that you need to start the Jarfile and pass the command to start your Minecraft Server as a runtime parameter. For example:
```
java -jar QuarantineBot.jar java -Xms1024M -Xmx4096M -jar server.jar --nogui
```
The first part `java -jar QuarantineBot.jar` will start the bot, the rest is passed as a parameter for the bot.
The parameter is then executed by the bot so that the bot has full control over the Standard Channels.

Configuration
-----
The bot will create a `quarantine.json` you need to fill with your bot credentials and information like the Discord Server and Channel you want it to operate in.
You'll need to create a Discord Bot Account over the [Discord Developers Portal](https://discordapp.com/developers/applications).
1. Click "New Application" in the top right corner
2. Give it a name and click "Create"
3. Click on "Bot" in the menu to the left
4. Click "Add Bot"
5. Click "Yes do it"

Now you need to invite the Bot to your server. Just go to the link below, but replace INSERT_CLIENT_ID_HERE with the Client ID you can find on the General Information Site on the Discord Developers Portal.
https://discordapp.com/oauth2/authorize?client_id=INSERT_CLIENT_ID_HERE&scope=bot&permissions=265216

You can create a dummy config file by running the Jarfile without parameters `java -jar QuarantineBot.jar`. 
All you need to do now is fill the token field with the token you can find on the Bot Menu in the Discord Developers Portal.
Here's a little explanation of what the other config fields do

| Name       | Description                                                                                       | Update Command     |
|------------|---------------------------------------------------------------------------------------------------|--------------------|
| serverId   | the Discord Server Identifier for the Server this Bot should operate in                           | !channel ¹         |
| channelId  | the Discord Channel to mirror into the Minecraft Server                                           | !channel ¹         |
| token      | the Discord Bot Token to start the Bot on                                                         | can't be updated   |
| prefix     | the Command Prefix, make sure to use something no other Bot on your server is currently using     | can't be updated   |
| deathEmoji | the Emoji the Bot will use to react to after every message indicating a player death in Minecraft | !emoji [Emoji]     |
| address    | the Minecraft Servers IP Address                                                                  | !ip [Address]      |

¹ has to be executed in the channel to use. 

Other Commands
-----

`!players`
lists the players currently on the server

`!ip`
posts the currently configured ip address

`!command`
allows the discord server owner to execute a command on the minecraft server

`!stop`
allows the discord server owner to stop the minecraft server

Contributing
-----
If you want to add a feature to the bot yourself just send me a pull request!
You'll need to [install Lombok](https://projectlombok.org/setup/overview) to your IDE of choice since I'm a lazy bastard. Otherwise the project will start off with a lot of compile errors.